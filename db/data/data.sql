CREATE DATABASE demo; 

USE demo;

CREATE TABLE customer (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    first_name CHAR(255),
    last_name CHAR(255),
    email CHAR(255),
    PRIMARY KEY (id)
);

INSERT INTO customer (first_name, last_name, email) VALUES
    ('Isaiah', 'Manning', 'ornare.Fusce@eleifend.edu'),
    ('Lester', 'Garrison', 'Curabitur@sagittisaugue.org'),
    ('Kate', 'Mccray', 'semper.Nam@lectusNullam.com'),
    ('Plato', 'Lucas', 'non@nectellus.co.uk'),
    ('Hilel', 'Gallegos', 'facilisi.Sed@et.edu');

CREATE USER '82618201'@'%' IDENTIFIED BY 'animism-sort-galvanic-angina-camp';
GRANT ALL PRIVILEGES ON demo.customer TO '82618201'@'%';