# If working with K/V v2
path "secret/data/myapp" {
    capabilities = ["read", "list"]
}

#added this to avoid application start error.
path "secret/data/application" {
    capabilities = ["read", "list"]
}
